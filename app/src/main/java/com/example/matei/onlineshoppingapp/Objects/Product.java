package com.example.matei.onlineshoppingapp.Objects;

import java.io.Serializable;

public class Product implements Serializable{

    private String name;
    private String description;
    private Double price;
    private String photoURL;

    public Product(String name, Double price, String description) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.photoURL = "https://png2.kisspng.com/sh/522d4678f9a09230363c163f0af86725/L0KzQYm3VcA2N5R9fZH0aYP2gLBuTfFvbKN0gdY2c3Xxg7F5TcVibZU8fdgAMEi2RLe8TsE1Pmo6SKU7MUW1RYa4U8k4O2E4S6g3cH7q/kisspng-android-sensor-5aed7ef50834f5.1469503215255139730336.png";
    }

    public Product(String name, Double price, String description, String photoURL) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.photoURL = photoURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }
}

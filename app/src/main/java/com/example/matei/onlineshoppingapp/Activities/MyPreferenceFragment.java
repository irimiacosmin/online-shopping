package com.example.matei.onlineshoppingapp.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.matei.onlineshoppingapp.R;

import static android.content.Context.MODE_PRIVATE;

public class MyPreferenceFragment extends PreferenceFragment
{
    public static final String KEY_LIST_PREFERENCE = "themePref";

    private ListPreference mListPreference;
    Context context;

    private void restartApp() {
        Intent intent = new Intent(context, MainActivity.class);
        int mPendingIntentId = 100;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }
    private void writeThemeToPrefs(String pref){
        String themeName = "red";
        switch (pref){
            case "1":{
                themeName = "red";
                break;
            }
            case "2":{
                themeName = "black";
                break;
            }
            case "3":{
                themeName = "blue";
                break;
            }
        }
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("themePreference", themeName).apply();




    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        context = this.getActivity().getApplicationContext();
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.fragment_preference);
        mListPreference = (ListPreference)getPreferenceScreen().findPreference(KEY_LIST_PREFERENCE);
        mListPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                writeThemeToPrefs(o.toString());
                return true;
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

        // Setup the initial values
//        mListPreference.setSummary("Current value is " + mListPreference.getEntry().toString());
//
//        // Set up a listener whenever a key changes
//        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister the listener whenever a key changes
        //getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

}
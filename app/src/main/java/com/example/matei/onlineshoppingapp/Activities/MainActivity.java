package com.example.matei.onlineshoppingapp.Activities;

import android.Manifest;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matei.onlineshoppingapp.Objects.Product;
import com.example.matei.onlineshoppingapp.Adapters.ProductAdapter;
import com.example.matei.onlineshoppingapp.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;


@RuntimePermissions
public class MainActivity extends AppCompatActivity {

    ListView productListView;
    ProductAdapter adapter;
    ArrayList<Product> productList;
    Context context;
    private boolean sortedByPrice = false;
    private boolean sortedByName = false;
    private boolean sortedAscending = true;
    private boolean sortedDescending = false;

    public void openDialog() {
        LayoutInflater factory = this.getLayoutInflater();
        final View dialogS = factory.inflate(R.layout.dialog_search, null);
        final EditText searchProduct = (EditText) dialogS.findViewById(R.id.searchProduct);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(dialogS)
                .setMessage("Search product")
                .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String text = searchProduct.getText().toString();
                        if(text.length() < 2){
                            Toast.makeText(context, "Enter 3 or more characters for search function to work.", Toast.LENGTH_LONG).show();
                        }else{
                            boolean flag = false;
                            for(Product product: productList){

                                if(product.getName().toLowerCase().contains(text.toLowerCase())){
                                    startProductActivity(product);
                                    flag = true;
                                    break;
                                }
                            }
                            if(!flag) {
                                for (Product product : productList) {
                                    if (product.getDescription().toLowerCase().contains(text.toLowerCase())) {
                                        startProductActivity(product);
                                        break;
                                    }
                                }
                            }
                            if(!flag){
                                Toast.makeText(context, "No results for the search phrase.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

        Dialog myDialog =  builder.create();
        myDialog.show();
    }

    private void sort(Comparator<Product> comparator){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            this.productList.sort(comparator);
            adapter.notifyDataSetChanged();
        }
    }

    private void orderAsc(){
        if(!sortedAscending){
            Collections.reverse(this.productList);
            sortedAscending = true;
            sortedDescending = false;
            adapter.notifyDataSetChanged();
        }
    }

    private void orderDesc(){
        if(!sortedDescending){
            Collections.reverse(this.productList);
            sortedAscending = false;
            sortedDescending = true;
            adapter.notifyDataSetChanged();
        }
    }

    private Comparator<Product> getNameComparator(){
        return new Comparator<Product>() {
            @Override
            public int compare(Product product, Product t1) {
                return product.getName().compareTo(t1.getName());
            }
        };
    }

    private Comparator<Product> getPriceComparator(){
        return new Comparator<Product>() {
            @Override
            public int compare(Product product, Product t1) {
                if(product.getPrice()>t1.getPrice())return 1;
                else if(product.getPrice()<t1.getPrice())return -1;
                return 0;
            }
        };
    }
    private void initViews(){
        populateListWithItems();
        context = this.getApplicationContext();
        productListView = this.findViewById(R.id.productListView);
        adapter = new ProductAdapter(productList, context);
    }
    private void populateListWithItems(){
        productList = new ArrayList<>();
        productList.add(new Product("Apple iPhone XS Max", 9490.0,"512GB, 4GB Ram, iOS 12", "https://s12emagst.akamaized.net/products/17738/17737192/images/res_0388968bb60a252748d56b50c2777108_450x450_4uqn.jpg"));
        productList.add(new Product("Samsung Galaxy S10+", 7799.99,"Dual SIM, 1TB, 12GB RAM, Android P","https://s12emagst.akamaized.net/products/20114/20113795/images/res_f591a35a7665cd7a24201ca7f4f5fefb_450x450_uq8e.jpg"));
        productList.add(new Product("Apple iPhone XR", 6240.0,"256GB, 3GB RAM, iOS 12","https://s12emagst.akamaized.net/products/18921/18920114/images/res_c2f9484efcabd26a8c6df53845719a11_450x450_5tmo.jpg"));
        productList.add(new Product("Huawei Mate 20 Pro", 6099.0,"128GB, Dual SIM, 6GB RAM, Android P","https://s12emagst.akamaized.net/products/17864/17863374/images/res_8a0d26a40d39391c17fc8b88dba93c9d_450x450_kglr.jpg"));
        productList.add(new Product("Google Pixel 3 XL", 5638.0,"64GB, 4GB RAM, Android P","https://s12emagst.akamaized.net/products/18884/18883367/images/res_60212f6b33195d13d83f0654733e4378_450x450_jc8u.jpg"));
        productList.add(new Product("Samsung Galaxy Note 9", 4940.0,"Dual Sim, 128GB, 6GB RAM, Android P","https://s12emagst.akamaized.net/products/20020/20019295/images/res_1dda00afd5c9a0fa8e96599d64f13812_450x450_vp4s.jpg"));
        productList.add(new Product("Apple iPhone X", 5460.0,"64GB, 3GB RAM, iOS 11","https://s12emagst.akamaized.net/products/15067/15066028/images/res_4d8708b99e28089f96a78c0f0c3e8362_450x450_jir5.jpg"));
        productList.add(new Product("Huawei Mate 20", 3900.0,"128GB, 4GB RAM, Android P","https://s12emagst.akamaized.net/products/19976/19975542/images/res_624e0e78864c9cfff1eda11fd2e8a352_450x450_lma7.jpg"));
        productList.add(new Product("BlackBerry Key 2", 3850.0,"128GB, 6GB RAM, Android O","https://s12emagst.akamaized.net/products/16027/16026268/images/res_4142c4a4b8f2dcdcb8d3f0d5c7021d92_450x450_v0ou.jpg"));
        productList.add(new Product("HTC U12 Plus", 3750.0,"64GB, 4GB RAM, Android O","https://s12emagst.akamaized.net/products/20212/20211987/images/res_22a8bd82c092274ceb84ac13e0cc9cc6_450x450_evrm.jpg"));
        productList.add(new Product("OnePlus 6T", 3640.0,"258GB, 8GB RAM, Android P","https://s12emagst.akamaized.net/products/20367/20366063/images/res_6ba6800d787b4909470f0d44e311e1e7_450x450_g4fc.jpg"));
        productList.add(new Product("Razer Phone 2", 3599.99,"64GB, 8GB RAM, Android P","https://s12emagst.akamaized.net/products/17873/17872029/images/res_c0fb4d54d9bc326657493be87faf4687_450x450_lj1r.jpg"));
        productList.add(new Product("LG V30 Plus", 3599.0,"128GB, 4GB RAM, Android N","https://s12emagst.akamaized.net/products/10692/10691420/images/res_8afd5c2c10322a0f65b61d284e3639f5_450x450_b2cf.jpg"));
        productList.add(new Product("Xiaomi Mi Mix 2S", 2860.0,"128GB, 6GB RAM, Android O","https://s12emagst.akamaized.net/products/20121/20120166/images/res_2331c02fbea095534d0da497d938ebbe_450x450_ll9u.jpg"));
        productList.add(new Product("Sony Xperia XZ2", 2859.0,"64GB, 4GB RAM, Android O","https://s12emagst.akamaized.net/products/16660/16659840/images/res_eec4a25e14ce2f62700cd785af586a8e_450x450_vh5a.jpg"));
    }

    public void checkTheme(String ceva){
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(this);
        String themeName = pref.getString("themePreference", "red");

        switch (themeName) {
                case "red": {
                    setTheme(R.style.AppTheme);
                    break;
                }
                case "black": {
                    setTheme(R.style.AppThemeBlack);
                    break;
                }
                case "blue": {
                    setTheme(R.style.AppThemeBlue);
                    break;
                }
                default: {
                    break;
                }
            }

    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        checkTheme("main");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivityPermissionsDispatcher.storageNeedWithPermissionCheck(this);
        MainActivityPermissionsDispatcher.giveLocationWithPermissionCheck(this);
        MainActivityPermissionsDispatcher.plsGiveCameraWithPermissionCheck(this);
        initViews();


        productListView.setAdapter(adapter);
        productListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Product product = productList.get(position);
                startProductActivity(product);
            }
        });
    }

    private void startProductActivity(Product product){
        Intent intent = new Intent(this, ProductActivity.class);
        intent.putExtra("product", (Serializable) product);
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.by_name: {
                sort(getNameComparator());
                return true;
            }case R.id.by_price: {
                sort(getPriceComparator());
                return true;
            }case R.id.ascending: {
                orderAsc();
                return true;
            }case R.id.descending: {
                orderDesc();
                return true;
            }case R.id.search: {
                openDialog();
                return true;
            }case R.id.sensors: {
                Intent intent = new Intent(this, SensorActivity.class);
                startActivity(intent);
                return true;
            }case R.id.preferences: {
                Intent intent = new Intent();
                intent.setClassName(this, "com.example.matei.onlineshoppingapp.Activities.MyPreferenceActivity");
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void storageNeed() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void storageShow(final PermissionRequest request) {
    }

    @OnPermissionDenied({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void storageDeny() {
    }

    @OnNeverAskAgain({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void storageNever() {
    }


    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void giveLocation() {
    }


    @NeedsPermission(Manifest.permission.CAMERA)
    void plsGiveCamera() {
    }
}

package com.example.matei.onlineshoppingapp.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matei.onlineshoppingapp.Objects.DownloadImage;
import com.example.matei.onlineshoppingapp.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CameraActivity extends AppCompatActivity {


    private Camera mCamera;
    private CameraPreview mPreview;
    private FrameLayout preview;
    private ImageView camera_img;
    private TextView camera_txt_progress;


    private byte[] picture = null;

    private boolean downloadImage(Bitmap bitmap){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + ".jpeg";

        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        File file = new File(path, imageFileName); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
        try {
            fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 70, fOut);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }


    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            setPicture(data);
            final Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
            camera_txt_progress.setVisibility(View.VISIBLE);
            camera_txt_progress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    camera_txt_progress.setText("Saving your photo. Please wait.");
                    camera_txt_progress.setOnClickListener(null);
                    boolean val = downloadImage(picture);
                    if(val){
                        Toast.makeText(getApplicationContext(),"Picture saved.",Toast.LENGTH_LONG).show();
                        camera_txt_progress.setVisibility(View.GONE);

                    }else{
                        Toast.makeText(getApplicationContext(),"ERROR: Picture cannot be saved.",Toast.LENGTH_LONG).show();
                        camera_txt_progress.setVisibility(View.GONE);
                    }

                }
            });

            camera_img.setImageBitmap(rotateBitmap(picture, 90));
            preview.setVisibility(View.GONE);
            camera_img.setVisibility(View.VISIBLE);

        }
    };

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }


    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] data) {
        picture = data;
    }


    private Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void previewListener() {
        mPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(null, null, mPicture);
                mPreview.setOnClickListener(null);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        camera_txt_progress = findViewById(R.id.camera_txt_progress);
        camera_img = findViewById(R.id.camera_img);

        mCamera = getCameraInstance();
        Camera.Parameters params = mCamera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        params.set("orientation", "portrait");
        params.set("jpeg-quality", 70);
        mCamera.setParameters(params);

        mPreview = new CameraPreview(this, mCamera);
        preview = findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        previewListener();

    }

}

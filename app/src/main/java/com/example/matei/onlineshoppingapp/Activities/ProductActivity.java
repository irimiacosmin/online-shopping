package com.example.matei.onlineshoppingapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matei.onlineshoppingapp.Objects.DownloadImage;
import com.example.matei.onlineshoppingapp.Objects.Product;
import com.example.matei.onlineshoppingapp.R;

public class ProductActivity extends AppCompatActivity {

    private TextView productTitle;
    private TextView productPrice;
    private TextView productDescription;
    private ImageView productImage;
    private Context context;

    public void checkTheme(){
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(this);
        String themeName = pref.getString("themePreference", "red");

            switch (themeName) {
                case "red": {
                    setTheme(R.style.AppTheme);
                    break;
                }
                case "black": {
                    setTheme(R.style.AppThemeBlack);
                    break;
                }
                case "blue": {
                    setTheme(R.style.AppThemeBlue);
                    break;
                }
                default: {
                    break;
                }
            }
    }


    private void init(){
        productTitle = this.findViewById(R.id.productName);
        productPrice = this.findViewById(R.id.productPrice);
        productDescription  = this.findViewById(R.id.productDescription);
        productImage = this.findViewById(R.id.productImage);
    }

    private void setThisToProduct(final Product product){
        new Thread() {
            public void run() {
                new DownloadImage(productImage).execute(product.getPhotoURL());
            }
        }.start();
        productTitle.setText(product.getName());
        productPrice.setText(product.getPrice().toString() + " lei");
        productDescription.setText(product.getDescription());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this.getApplicationContext();
        checkTheme();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        init();
        Intent intent = getIntent();
        //String myString = intent.getStringExtra(Intent.EXTRA_TEXT);
        //Toast.makeText(getApplicationContext(), myString, Toast.LENGTH_LONG).show();
        Product product = (Product) intent.getSerializableExtra("product");
        setThisToProduct(product);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCameraActivity();
            }
        });
    }

    private void startCameraActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
        this.finish();
    }

}

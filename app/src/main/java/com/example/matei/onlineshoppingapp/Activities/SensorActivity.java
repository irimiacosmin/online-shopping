package com.example.matei.onlineshoppingapp.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.matei.onlineshoppingapp.Adapters.ProductAdapter;
import com.example.matei.onlineshoppingapp.Objects.Product;
import com.example.matei.onlineshoppingapp.R;

import java.util.ArrayList;
import java.util.List;

public class SensorActivity extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;
    private GPSTracker gps;

    ProductAdapter adapter;
    ArrayList<Product> productList;
    ListView sensorList;

    public void checkTheme(){
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(this);
        String themeName = pref.getString("themePreference", "red");

        switch (themeName) {
            case "red": {
                setTheme(R.style.AppTheme);
                break;
            }
            case "black": {
                setTheme(R.style.AppThemeBlack);
                break;
            }
            case "blue": {
                setTheme(R.style.AppThemeBlue);
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        checkTheme();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sensor);

        sensorList = this.findViewById(R.id.sensorList);
        productList = new ArrayList<>();

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        List<Sensor> list = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        for(Sensor sensor: list){
            productList.add(new Product(sensor.getName(),(double)sensor.getPower(),"Vendor: "+ sensor.getVendor() + "  | Version: " + sensor.getVersion()));
        }
        adapter = new ProductAdapter(productList, getApplicationContext());
        sensorList.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gps = new GPSTracker(SensorActivity.this);

                // Check if GPS enabled
                if(gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    // \n is for new line
                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // Can't get location.
                    // GPS or network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    gps.showSettingsAlert();
                }

            }
        });
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
    }
}

package com.example.matei.onlineshoppingapp.Objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class DownloadImage extends AsyncTask<String, Void, Bitmap> {
    private ImageView _image;
    public DownloadImage(ImageView image) {
        _image = image;
    }
    @Override
    protected Bitmap doInBackground(String... params) {
        String url = params[0];
        String md5 = Utils.md5(url);
        Bitmap bitmap = checkForExistingFile(md5);
        if(bitmap == null) {
            try {
                InputStream in = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(in);
                downloadImage(md5, bitmap);
            } catch (Exception e) {}
        }
        return bitmap;
    }

    private Bitmap checkForExistingFile(String url){
        File root = Environment.getExternalStorageDirectory();
        try {
            Bitmap bMap = BitmapFactory.decodeFile(root + "/" + url + ".png");
            return bMap;
        }catch (Exception e){
            return null;
        }
    }

    private void downloadImage(String name, Bitmap bitmap){
        File root = Environment.getExternalStorageDirectory();
        try (FileOutputStream out = new FileOutputStream(root+"/"+name+".png")) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 70, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onPostExecute(Bitmap result) {
        if (result != null)
            _image.setImageBitmap(result);
    }
}

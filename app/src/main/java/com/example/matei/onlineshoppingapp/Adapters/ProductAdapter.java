package com.example.matei.onlineshoppingapp.Adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matei.onlineshoppingapp.Objects.DownloadImage;
import com.example.matei.onlineshoppingapp.Objects.Product;
import com.example.matei.onlineshoppingapp.Objects.Utils;
import com.example.matei.onlineshoppingapp.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ProductAdapter extends ArrayAdapter<Product> implements View.OnClickListener {

    private ArrayList<Product> products;
    private Context mContext;

    public ProductAdapter(ArrayList<Product> data, Context context) {
        super(context, R.layout.product_layout, data);
        this.products = data;
        this.mContext = context;

    }

    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag();
        Object object = getItem(position);
        Product dataModel = (Product) object;

        switch (v.getId()) {
            case R.id.item_info: {
                Toast.makeText(mContext, "You got the first Easter Egg. 14 more to get the prize.", Toast.LENGTH_LONG).show();
                break;
            }
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Product product = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.product_layout, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtType = convertView.findViewById(R.id.description);
            viewHolder.picture = convertView.findViewById(R.id.item_info);
            viewHolder.price = convertView.findViewById(R.id.price);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtName.setText(product.getName());
        viewHolder.txtType.setText(product.getDescription());
        String price = product.getPrice().toString();
        if(price.endsWith(".0")){
            price += "0";
        }



        if (product.getPhotoURL() != null && viewHolder.picture.getTag() == null) {
            if(product.getPhotoURL().startsWith("https://png2.kisspng.com/sh/522")){
                viewHolder.price.setText("Power: " + price);
            }else{
                viewHolder.price.setText(price + "lei");
            }

            new Thread() {
                public void run() {
                    new DownloadImage(viewHolder.picture).execute(product.getPhotoURL());
                }
            }.start();
        }

        return convertView;
    }

    private static class ViewHolder {
        ImageView picture;
        TextView txtName;
        TextView txtType;
        TextView price;
    }


}

